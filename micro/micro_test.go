package micro

import (
	"testing"
	"time"

	"gitlab.com/elaina/std-go/rpc"
)

type Demo struct{}

func (d *Demo) A(a int, b *string) error {
	return nil
}

func (d *Demo) B(a int, b *int) error {
	return nil
}

func TestMicro(t *testing.T) {
	go func() {
		time.Sleep(1 * time.Second)
		rpcs := rpc.NewServer()
		rpcs.Register(&Demo{})
		cli, _ := rpc.Dial(":8000", &rpc.DefaultOption)
		s := NewMicroServer(rpcs, cli, "Registry.Register", 5*time.Second)
		s.Run(":8002", false)
	}()
	go func() {
		time.Sleep(1 * time.Second)
		rpcs := rpc.NewServer()
		rpcs.Register(&Demo{})
		cli, _ := rpc.Dial(":8000", &rpc.DefaultOption)
		s := NewMicroServer(rpcs, cli, "Registry.Register", 7*time.Second)
		s.Run(":8003", false)
	}()
	m := NewMicro(&DefaultOption)
	m.Run()
}
