package registry

import (
	"errors"
	"fmt"
	"sync"
	"time"
)

type RegisterReq struct {
	Addr           string
	ServiceMethods map[string]string
}

type RegisterResp struct {
	Success bool
	Message string
}

type ApplyServiceInstanceReq struct {
	Mode    SelectMode
	Service string
}

type ApplyServiceInstanceResp struct {
	Addr string
}

func NewRegistry(gap time.Duration, timeout int) *Registry {
	return &Registry{
		gap:         gap,
		timeout:     timeout,
		servicesMap: sync.Map{},
		closed:      make(chan interface{}, 1),
	}
}

type Registry struct {
	gap         time.Duration
	timeout     int
	servicesMap sync.Map
	closed      chan interface{}
}

func (r *Registry) Register(req RegisterReq, resp *RegisterResp) error {
	for name, methods := range req.ServiceMethods {
		s, ok := r.servicesMap.Load(name)
		if !ok {
			s = newService(name, r.timeout, methods)
			r.servicesMap.Store(name, s)
		}
		if !s.(*service).match(name, methods) {
			return fmt.Errorf("service name{%s} does not match methods", name)
		}
		s.(*service).update(req.Addr)
	}
	return nil
}

func (r *Registry) ListServices(req interface{}, resp map[string]string) error {
	r.servicesMap.Range(func(key, value any) bool {
		resp[key.(string)] = value.(*service).methods
		return true
	})
	return nil
}

func (r *Registry) ApplyServiceInstance(req ApplyServiceInstanceReq, resp *ApplyServiceInstanceResp) error {
	s, ok := r.servicesMap.Load(req.Service)
	if !ok {
		return errors.New("invalid service " + req.Service)
	}
	addr, err := s.(*service).getInstance(req.Mode)
	resp.Addr = addr
	return err
}

func (r *Registry) CheckAlive() {
	for {
		switch {
		case <-r.closed:
			return
		default:
			r.servicesMap.Range(func(key, value any) bool {
				s, ok := r.servicesMap.Load(key)
				if !ok {
					return false
				}
				s.(*service).checkAlive()
				return true
			})
		}
		time.Sleep(r.gap)
	}
}

func (r *Registry) Close() {
	r.closed <- struct{}{}
}
