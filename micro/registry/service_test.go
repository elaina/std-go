package registry

import (
	"fmt"
	"testing"
	"time"
)

func TestService(t *testing.T) {
	done := make(chan struct{}, 1)
	s := newService("DemoService", 4, "")
	go func(s *service, done <-chan struct{}) {
		for {
			select {
			case <-done:
				return
			default:
				s.checkAlive()
			}
		}
	}(s, done)
	fmt.Printf("[1]\ts.getInstancesList(): %v\n", s.getInstancesList())
	addrs := []string{
		"127.0.0.1:8000",
		"127.0.0.1:8001",
		"127.0.0.1:8002",
		"127.0.0.1:8003",
		"127.0.0.1:8004",
	}
	for i := 0; i < 5; i++ {
		s.update(addrs[i])
	}
	fmt.Printf("[2]\ts.getInstancesList(): %v\n", s.getInstancesList())
	time.Sleep(2 * time.Second)
	s.update(addrs[1])
	s.update(addrs[3])
	time.Sleep(3 * time.Second)
	fmt.Printf("[3]\ts.getInstancesList(): %v\n", s.getInstancesList())
	s.update(addrs[0])
	fmt.Printf("[4]\ts.getInstancesList(): %v\n", s.getInstancesList())
	done <- struct{}{}
}
