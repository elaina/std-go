package registry

import (
	"fmt"
	"log"
	"math/rand"
	"sync"
	"sync/atomic"
	"time"
)

type SelectMode int

const (
	RandomSelect     SelectMode = 0
	RoundRobinSelect SelectMode = 1
)

type instance struct {
	addr string
	hb   int64
}

func newService(name string, timeout int, methods string) *service {
	return &service{
		name:      name,
		timeout:   int64(timeout),
		mu:        &sync.RWMutex{},
		addrs:     []string{},
		instances: make(map[string]*instance),
		round:     0,
		methods:   methods,
	}
}

type service struct {
	name      string
	timeout   int64
	mu        *sync.RWMutex
	addrs     []string
	instances map[string]*instance
	round     int32
	methods   string
}

func (s *service) match(name string, methods string) bool {
	return s.name == name && s.methods == methods
}

func (s *service) update(addr string) {
	s.mu.Lock()
	defer s.mu.Unlock()
	in, ok := s.instances[addr]
	if !ok {
		in = &instance{addr: addr, hb: time.Now().Unix()}
		s.addrs = append(s.addrs, addr)
		s.instances[addr] = in
	}
	in.hb = time.Now().Unix()
}

func (s *service) checkAlive() {
	s.mu.Lock()
	defer s.mu.Unlock()
	for addr, in := range s.instances {
		if time.Now().Unix()-in.hb > s.timeout {
			for i := 0; i < len(s.addrs); i++ {
				if s.addrs[i] == addr {
					s.addrs = append(s.addrs[:i], s.addrs[i+1:]...)
					break
				}
			}
			delete(s.instances, addr)
			log.Printf("std-go/micro service{%s} instance@%s is unalive\n", s.name, addr)
		}
	}
}

func (s *service) getInstance(mode SelectMode) (string, error) {
	switch mode {
	case RandomSelect:
		i := rand.Intn(len(s.addrs))
		return s.addrs[i%len(s.addrs)], nil
	case RoundRobinSelect:
		i := atomic.AddInt32(&s.round, 1)
		return s.addrs[int(i)%len(s.addrs)], nil
	default:
		return "", fmt.Errorf("invalid select mode code{%d}", mode)
	}
}

func (s *service) getInstancesList() []string {
	s.mu.RLock()
	defer s.mu.RUnlock()
	res := make([]string, 0, len(s.instances))
	for k := range s.instances {
		res = append(res, k)
	}
	return res
}
