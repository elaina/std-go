package micro

import (
	"log"
	"net"
	"net/http"
	"time"

	"gitlab.com/elaina/std-go/micro/registry"
	"gitlab.com/elaina/std-go/rpc"
	"gitlab.com/elaina/std-go/web"
)

type Option struct {
	AddrRPC  string
	AddrHTTP string
	Gap      time.Duration
	Timeout  int
}

var DefaultOption = Option{
	AddrRPC:  ":8000",
	AddrHTTP: ":8001",
	Gap:      5,
	Timeout:  10,
}

func NewMicro(opt *Option) *Micro {
	return &Micro{
		AddrRPC:        opt.AddrRPC,
		AddrHTTP:       opt.AddrHTTP,
		rgistryService: registry.NewRegistry(opt.Gap, opt.Timeout),
	}
}

type Micro struct {
	AddrRPC        string
	AddrHTTP       string
	rgistryService *registry.Registry
}

func (m *Micro) Run() {
	go m.rgistryService.CheckAlive()
	go m.startRPCServer()

	httpServer := web.New()
	httpServer.Use(web.Logger, web.Recovery)
	microGroup := httpServer.Group("/std-go/micro")
	microGroup.Get("/registry", func(ctx *web.Context) {
		res := make(map[string]string)
		m.rgistryService.ListServices(nil, res)
		ctx.JSON(http.StatusOK, web.H{
			"success": true,
			"message": "success",
			"res":     res,
		})
	})
	httpServer.Run(m.AddrHTTP)
}

func (m *Micro) startRPCServer() {
	listener, err := net.Listen("tcp", m.AddrRPC)
	if err != nil {
		log.Println(err)
		return
	}
	rpcServer := rpc.NewServer()
	rpcServer.Register(m.rgistryService)
	rpcServer.Serve(listener)
}
