package micro

import (
	"log"
	"net"
	"time"

	"gitlab.com/elaina/std-go/micro/registry"
	"gitlab.com/elaina/std-go/rpc"
)

func NewMicroServer(server *rpc.Server, client *rpc.Client, call string, gap time.Duration) *MicroServer {
	return &MicroServer{
		rpcServer: server,
		rpcClient: client,
		call:      call,
		gap:       gap,
	}
}

type MicroServer struct {
	rpcServer *rpc.Server
	rpcClient *rpc.Client
	call      string
	gap       time.Duration
}

func (m *MicroServer) heartBeat(addr string) {
	req := registry.RegisterReq{
		Addr:           addr,
		ServiceMethods: m.rpcServer.ListServices(),
	}
	for {
		m.rpcClient.Call(m.call, req, &registry.RegisterResp{})
		time.Sleep(m.gap)
	}
}

func (m *MicroServer) Run(addr string, http bool) {
	go m.heartBeat(addr)
	listener, err := net.Listen("tcp", addr)
	if err != nil {
		log.Println(err)
		return
	}
	if http {
		m.rpcServer.ServeHTTP(listener)
	} else {
		m.rpcServer.Serve(listener)
	}
}
