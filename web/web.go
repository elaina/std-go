package web

import (
	"html/template"
	"net/http"
	"path"
	"strings"

	"gitlab.com/elaina/std-go/web/enum/method"
	"gitlab.com/elaina/std-go/web/trie"
)

type HandlerFunc func(*Context)

type MiddleWares []HandlerFunc

type H map[string]interface{}

type Engine struct {
	router   Router
	mwKeys   []string               // 单独存储中间件的绑定URL前缀，以保证后续的顺序遍历
	mwMap    map[string]MiddleWares // 可以分组定制中间件
	template *template.Template
	funcMap  *template.FuncMap
}

func (e *Engine) addRoute(method string, pattern string, handler HandlerFunc) {
	e.router.addRoute(method, pattern, handler)
}

func (e *Engine) use(prefix string, middlewares ...HandlerFunc) {
	if _, ok := e.mwMap[prefix]; !ok {
		e.mwKeys = append(e.mwKeys, prefix)
	}
	e.mwMap[prefix] = append(e.mwMap[prefix], middlewares...)
}

func (e *Engine) Use(middlewares ...HandlerFunc) {
	e.use("/", middlewares...)
}

func (e *Engine) SetFuncMap(funcMap *template.FuncMap) {
	e.funcMap = funcMap
}

func (e *Engine) LoadHTMLGlob(pattern string) {
	e.template = template.Must(template.New("").Funcs(*e.funcMap).ParseGlob(pattern))
}

func (e *Engine) Group(prefix string) *RouteGroup {
	if !strings.HasPrefix(prefix, "/") {
		prefix = "/" + prefix
	}
	return &RouteGroup{prefix: prefix, engine: e}
}

func (e *Engine) Get(pattern string, handler HandlerFunc) {
	e.addRoute(method.GET, pattern, handler)
}

func (e *Engine) Post(pattern string, handler HandlerFunc) {
	e.addRoute(method.POST, pattern, handler)
}

func (e *Engine) Static(pattern string, root string) {
	fs := http.Dir(root)
	fileServer := http.StripPrefix(pattern, http.FileServer(fs))
	pattern = path.Join(pattern, "/*uri")
	e.Get(pattern, func(ctx *Context) {
		uri := ctx.Param("uri")
		if _, err := fs.Open(uri); err != nil {
			ctx.setStatusCode(http.StatusNotFound)
			return
		}
		fileServer.ServeHTTP(ctx.Response, ctx.Request)
	})

}

func (e *Engine) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	ctx := NewContext(e, w, req)
	for _, prefix := range e.mwKeys {
		if strings.HasPrefix(ctx.GetURL(), prefix) {
			ctx.addHandlers(e.mwMap[prefix]...)
		}
	}
	e.router.handle(ctx)
}

func (e *Engine) Run(addr string) error {
	return http.ListenAndServe(addr, e)
}

func New() *Engine {
	return &Engine{
		router: Router{
			patterns: trie.New(),
			routeMap: make(map[string]HandlerFunc),
		},
		mwKeys: []string{},
		mwMap:  make(map[string]MiddleWares),
	}
}

func Default() *Engine {
	engine := New()
	engine.Use(Logger, Recovery)
	return engine
}
