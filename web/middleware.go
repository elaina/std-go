package web

import (
	"fmt"
	"runtime"
	"time"
)

func Logger(ctx *Context) {
	start := time.Now()
	ctx.Next()
	fmt.Printf("%s %s %s %dms\n", start.Format("[2006/01/02 15:04:05]"), ctx.GetMethod(), ctx.GetURL(), time.Since(start).Milliseconds())
}

func Recovery(ctx *Context) {
	defer func() {
		if err := recover(); err != nil {
			buf := make([]byte, 2048)
			n := runtime.Stack(buf, false)
			fmt.Printf("%s", buf[:n])
		}
	}()
}
