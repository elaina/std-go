package web

import "strings"

type RouteGroup struct {
	prefix string
	engine *Engine
}

func (g *RouteGroup) Use(middlewares ...HandlerFunc) {
	g.engine.use(g.prefix, middlewares...)
}

func (g *RouteGroup) Group(prefix string) *RouteGroup {
	if !strings.HasPrefix(prefix, "/") {
		prefix = "/" + prefix
	}
	return &RouteGroup{prefix: g.prefix + prefix, engine: g.engine}
}

func (g *RouteGroup) Get(pattern string, handler HandlerFunc) {
	if !strings.HasPrefix(pattern, "/") {
		pattern = "/" + pattern
	}
	g.engine.Get(g.prefix+pattern, handler)
}

func (g *RouteGroup) Post(pattern string, handler HandlerFunc) {
	if !strings.HasPrefix(pattern, "/") {
		pattern = "/" + pattern
	}
	g.engine.Post(g.prefix+pattern, handler)
}

func (g *RouteGroup) Static(pattern string, root string) {
	if !strings.HasPrefix(pattern, "/") {
		pattern = "/" + pattern
	}
	g.engine.Static(g.prefix+pattern, root)
}
