package web

import (
	"fmt"
	"net/http"

	"gitlab.com/elaina/std-go/web/trie"
)

type Router struct {
	patterns trie.PatternTree
	routeMap map[string]HandlerFunc
}

func (r *Router) addRoute(method string, pattern string, handler HandlerFunc) {
	r.patterns.Add(pattern)
	key := method + ":" + pattern
	r.routeMap[key] = handler
}

func (r *Router) handle(ctx *Context) {
	pattern, params, _ := r.patterns.Match(ctx.GetURL())
	key := ctx.GetMethod() + ":" + pattern
	if handler, ok := r.routeMap[key]; ok {
		ctx.addHandlers(handler)
		ctx.setParams(params)
		ctx.Next() // 开始执行中间件
	} else {
		ctx.setStatusCode(http.StatusNotFound)
		fmt.Fprintf(ctx.Response, "404 Not Found: %s", ctx.GetURL())
	}
}
