package web

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/elaina/std-go/web/enum/ctype"
	"gitlab.com/elaina/std-go/web/enum/header"
)

type Context struct {
	engine   *Engine
	Response http.ResponseWriter
	Request  *http.Request
	params   map[string]string
	handlers []HandlerFunc
	index    int
}

func (c *Context) addHandlers(middlewares ...HandlerFunc) {
	c.handlers = append(c.handlers, middlewares...)
}

func (c *Context) setStatusCode(statusCode int) {
	c.Response.WriteHeader(statusCode)
}

func (c *Context) setHeader(key string, value string) {
	c.Response.Header().Set(key, value)
}

func (c *Context) setParams(params map[string]string) {
	c.params = params
}

func (c *Context) GetMethod() string {
	return c.Request.Method
}

func (c *Context) GetURL() string {
	return c.Request.URL.Path
}

func (c *Context) Next() {
	c.index++
	for ; c.index < len(c.handlers); c.index++ {
		c.handlers[c.index](c)
	}
}

func (c *Context) Query(key string) string {
	return c.Request.URL.Query().Get(key)
}

func (c *Context) Form(key string) string {
	return c.Request.FormValue(key)
}

func (c *Context) Param(key string) string {
	return c.params[key]
}

func (c *Context) Print(statusCode int, msg string) {
	c.setHeader(header.CONTENT_TYPE, ctype.PLAIN)
	c.setStatusCode(statusCode)
	c.Response.Write([]byte(msg))
}

func (c *Context) Printf(statusCode int, format string, args ...interface{}) {
	c.setHeader(header.CONTENT_TYPE, ctype.PLAIN)
	c.setStatusCode(statusCode)
	c.Response.Write([]byte(fmt.Sprintf(format, args...)))
}

func (c *Context) JSON(statusCode int, data interface{}) {
	c.setHeader(header.CONTENT_TYPE, ctype.JSON)
	c.setStatusCode(statusCode)
	encoder := json.NewEncoder(c.Response)
	if err := encoder.Encode(data); err != nil {
		// 下面的代码没有用，因为前面已经调用过了Header().Set(), WriteHeader(), Write()三个函数，http.Error中重复调用将会失效
		// http.Error(c.Response, err.Error(), http.StatusInternalServerError)
		// 下面是仿照gin框架的处理方式，感觉不太合适，但也没啥其他办法了
		panic(err)
	}
}

func (c *Context) HTML(statusCode int, html string) {
	c.setHeader(header.CONTENT_TYPE, ctype.HTML)
	c.setStatusCode(statusCode)
	c.Response.Write([]byte(html))
}

func (c *Context) Template(statusCode int, template string, data interface{}) {
	c.setHeader(header.CONTENT_TYPE, ctype.HTML)
	c.setStatusCode(statusCode)
	if err := c.engine.template.ExecuteTemplate(c.Response, template, data); err != nil {
		c.setStatusCode(http.StatusInternalServerError)
		fmt.Println(err)
	}
}

func NewContext(e *Engine, w http.ResponseWriter, req *http.Request) *Context {
	return &Context{
		engine:   e,
		Response: w,
		Request:  req,
		handlers: []HandlerFunc{},
		index:    -1,
	}
}
