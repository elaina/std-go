package trie

import (
	"strings"
)

type TrieNode struct {
	token  string
	fuzzy  bool
	any    bool
	childs []*TrieNode
}

func (n *TrieNode) AddChild(node *TrieNode) {
	n.childs = append(n.childs, node)
}

func (n *TrieNode) GetChild(token string) *TrieNode {
	for _, node := range n.childs {
		if node.token == token {
			return node
		}
	}
	return nil
}

func (n *TrieNode) MatchChilds(token string) []*TrieNode {
	matched := []*TrieNode{}
	for _, node := range n.childs {
		if node.token == token || node.fuzzy || node.any {
			matched = append(matched, node)
		}
	}
	return matched
}

type PatternTree struct {
	root *TrieNode
}

func (t *PatternTree) Add(pattern string) {
	pattern = strings.TrimPrefix(pattern, "/")
	tokens := strings.Split(pattern, "/")
	node := t.root
	for _, token := range tokens {
		next := node.GetChild(token)
		if next == nil {
			// a new sub-url
			next = &TrieNode{
				token:  token,
				fuzzy:  strings.HasPrefix(token, ":"),
				any:    strings.HasPrefix(token, "*"),
				childs: []*TrieNode{},
			}
			node.AddChild(next)
		}
		node = next
	}
}

func (t *PatternTree) match(node *TrieNode, tokens []string, params map[string]string) (int, bool) {
	if len(tokens) == 0 {
		return 0, true
	}
	matched := node.MatchChilds(tokens[0])
	for _, m := range matched {
		if m.any {
			params[m.token[1:]] = "/" + strings.Join(tokens, "/")
			tokens[0] = m.token
			return 1, true
		}
		len, ok := t.match(m, tokens[1:], params)
		if ok {
			if m.fuzzy {
				params[m.token[1:]] = tokens[0]
			}
			tokens[0] = m.token
			return len + 1, true
		}
	}
	return 0, false
}

func (t *PatternTree) Match(url string) (string, map[string]string, bool) {
	url = strings.TrimPrefix(url, "/")
	tokens := strings.Split(url, "/")
	params := make(map[string]string)
	len, ok := t.match(t.root, tokens, params)
	return "/" + strings.Join(tokens[:len], "/"), params, ok
}

func New() PatternTree {
	return PatternTree{root: &TrieNode{token: "/", childs: []*TrieNode{}}}
}
