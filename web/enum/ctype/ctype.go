package ctype

const (
	PLAIN = "text/plain"
	HTML  = "text/html"
	JSON  = "application/json"
)
