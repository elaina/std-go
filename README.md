### STD-Go

#### 1. Introduction

STD-GO，Personal Standard Library for Golang

包含一些个人常用的函数、类、方法以及代码模版。

#### 2. Packages

* crypto：常用的加密和解密算法
* dao：基于gorm提供数据库操作支持
* mail：基于jordan-wright/email提供邮件服务
* webserver：快速构建基于gin框架的简单web服务器
* web：自定义的轻量级web框架
* rpc：自定义的轻量级rpc框架
* micro：自定义的微服务框架，提供服务注册、服务发现、负载均衡等基本功能
* cache：自定义的分布式缓存，开发中