package codec

import (
	"io"

	"gitlab.com/elaina/std-go/rpc/protoc"
)

type CodecConn interface {
	io.Closer
	Write(*protoc.Header, interface{}) error
	ReadHeader(*protoc.Header) error
	ReadBody(interface{}) error
}

type CodecType string

type NewCodecConn func(io.ReadWriteCloser) CodecConn

const (
	GOB  CodecType = "application/gob"
	JSON CodecType = "application/json"
)

var NewCodecConnFunc map[CodecType]NewCodecConn

func init() {
	NewCodecConnFunc = make(map[CodecType]NewCodecConn)
	NewCodecConnFunc[GOB] = NewGobCodecConn
	NewCodecConnFunc[JSON] = NewJsonCodecConn
}
