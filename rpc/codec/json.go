package codec

import (
	"encoding/json"
	"io"

	"gitlab.com/elaina/std-go/rpc/protoc"
)

func NewJsonCodecConn(conn io.ReadWriteCloser) CodecConn {
	return &JsonCodecConn{
		conn:    conn,
		encoder: json.NewEncoder(conn),
		decoder: json.NewDecoder(conn),
	}
}

type JsonCodecConn struct {
	conn    io.ReadWriteCloser
	encoder *json.Encoder
	decoder *json.Decoder
}

func (jc *JsonCodecConn) Write(header *protoc.Header, body interface{}) error {
	if err := jc.encoder.Encode(header); err != nil {
		return err
	}
	if err := jc.encoder.Encode(body); err != nil {
		return err
	}
	return nil
}

func (jc *JsonCodecConn) ReadHeader(header *protoc.Header) error {
	return jc.decoder.Decode(header)
}

func (jc *JsonCodecConn) ReadBody(body interface{}) error {
	return jc.decoder.Decode(body)
}

func (jc *JsonCodecConn) Close() error {
	return jc.conn.Close()
}
