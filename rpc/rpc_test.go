package rpc

import (
	"fmt"
	"log"
	"net"
	"sync"
	"testing"
)

type Demo struct {
	Text string
}

func (d *Demo) A() error {
	fmt.Println("A")
	return nil
}

func (d *Demo) B(a string, b *string) error {
	fmt.Println(a)
	return nil
}

func (d *Demo) C(a string, b *Demo) error {
	fmt.Println(a)
	b.Text = "Hello, World!"
	return nil
}

func (d *Demo) D(a string, b Demo) error {
	fmt.Println(a)
	return nil
}

func TestRPC(t *testing.T) {
	addr := make(chan string, 1)
	go func(addr chan<- string) {
		lis, _ := net.Listen("tcp", ":8000")
		addr <- lis.Addr().String()
		rpcServer := NewServer()
		rpcServer.Register(&Demo{})
		rpcServer.ServeHTTP(lis)
	}(addr)

	client, err := DialHTTP(<-addr, &DefaultOption)
	if err != nil {
		log.Println(err)
		return
	}
	defer client.Close()

	wg := &sync.WaitGroup{}
	for i := 0; i < 5; i++ {
		wg.Add(1)
		go func(wg *sync.WaitGroup, i int) {
			defer wg.Done()
			reply := Demo{Text: "demo"}
			if err := client.Call("Demo.C", fmt.Sprintf("test%d", i), &reply); err != nil {
				log.Println(err)
			} else {
				log.Println("调用成功", i)
			}
		}(wg, i)
	}
	wg.Wait()
}
