package rpc

import (
	"log"
	"reflect"
)

type method struct {
	name      string
	method    reflect.Method
	argType   reflect.Type
	replyType reflect.Type
}

func (m *method) newArg() reflect.Value {
	if m.argType.Kind() == reflect.Pointer {
		return reflect.New(m.argType.Elem())
	} else {
		return reflect.New(m.argType).Elem()
	}
}

func (m *method) newReply() reflect.Value {
	reply := reflect.New(m.replyType.Elem())
	switch m.replyType.Elem().Kind() {
	case reflect.Map:
		reply.Elem().Set(reflect.MakeMap(m.replyType.Elem()))
	case reflect.Slice:
		reply.Elem().Set(reflect.MakeSlice(m.replyType.Elem(), 0, 0))
	}
	return reply
}

func newService(s interface{}) *service {
	refValue := reflect.ValueOf(s)
	refType := refValue.Type()
	name := reflect.Indirect(refValue).Type().Name()
	svc := service{
		name:       name,
		refType:    refType,
		refValue:   refValue,
		methodsMap: make(map[string]*method),
	}
	svc.registerMethods()
	return &svc
}

type service struct {
	name       string
	refType    reflect.Type
	refValue   reflect.Value
	methodsMap map[string]*method
}

func (s *service) registerMethods() {
	for i := 0; i < s.refType.NumMethod(); i++ {
		mtd := s.refType.Method(i)
		// example: func (e *Example) MethodName(req Request, resp *Response) error
		mType := mtd.Type
		if mType.NumIn() != 3 || mType.NumOut() != 1 {
			// check the num of parameters
			continue
		}
		if mType.Out(0) != reflect.TypeOf((*error)(nil)).Elem() {
			// check the type of out parameter
			continue
		}
		argType, replyType := mType.In(1), mType.In(2)
		if replyType.Kind() != reflect.Pointer && replyType.Kind() != reflect.Map && replyType.Kind() != reflect.Slice {
			continue
		}
		s.methodsMap[mtd.Name] = &method{
			name:      mtd.Name,
			method:    mtd,
			argType:   argType,
			replyType: replyType,
		}
		log.Printf("std-go/rpc server: register %s.%s\n", s.name, mtd.Name)
	}
}

func (s *service) call(methodName string, args reflect.Value, reply reflect.Value) error {
	method := s.methodsMap[methodName].method.Func
	res := method.Call([]reflect.Value{
		s.refValue,
		args,
		reply,
	})[0].Interface()
	if res == nil {
		return nil
	}
	return res.(error)
}
