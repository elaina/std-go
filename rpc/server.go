package rpc

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"reflect"
	"strings"
	"sync"
	"time"

	"gitlab.com/elaina/std-go/rpc/codec"
	"gitlab.com/elaina/std-go/rpc/protoc"
)

const (
	MAGIC         = 0xff67a0
	HTTP_PROTOCOL = "HTTP/1.0"
	HTTP_URL      = "/std-go/rpc"
	HTTP_METHOD   = "CONNECT"
)

type Option struct {
	Magic          int
	CodecType      codec.CodecType
	ConnectTimeout time.Duration
	HandleTimeout  time.Duration
}

var DefaultOption = Option{
	Magic:          MAGIC,
	CodecType:      codec.GOB,
	ConnectTimeout: 10 * time.Second,
	HandleTimeout:  10 * time.Second,
}

func NewServer() *Server {
	return &Server{
		servicesMap: make(map[string]*service),
	}
}

type Server struct {
	servicesMap map[string]*service
}

func (s *Server) Register(svcp interface{}) {
	svc := newService(svcp)
	s.servicesMap[svc.name] = svc
}

func (s *Server) Serve(listener net.Listener) {
	for {
		conn, err := listener.Accept()
		if err != nil {
			fmt.Println(err)
			return
		}
		go s.serveConn(conn)
	}
}

func (s *Server) ServeHTTP(listener net.Listener) {
	s.ServeHTTPURL(HTTP_URL, listener)
}

func (s *Server) ServeHTTPURL(url string, listener net.Listener) {
	http.HandleFunc(url, s.httpHandler)
	http.Serve(listener, nil)
}

func (s *Server) ListServices() map[string]string {
	res := make(map[string]string)
	builder := strings.Builder{}
	for _, svc := range s.servicesMap {
		for _, mtd := range svc.methodsMap {
			mType := mtd.method.Type
			builder.WriteString(fmt.Sprintf("%s(%s, *%s) %s\n",
				mtd.name,
				mType.In(1).Name(),
				mType.In(2).Elem().Name(),
				mType.Out(0).Name()),
			)
		}
		res[svc.name] = builder.String()
		builder.Reset()
	}
	return res
}

func (s *Server) httpHandler(w http.ResponseWriter, req *http.Request) {
	if req.Method != HTTP_METHOD {
		w.Header().Set("Content-Type", "text/plain; charset=utf-8")
		w.WriteHeader(http.StatusMethodNotAllowed)
		io.WriteString(w, fmt.Sprintf("must be %s", HTTP_METHOD))
		return
	}
	conn, _, err := w.(http.Hijacker).Hijack()
	if err != nil {
		log.Println("rpc hijack error:", err)
		return
	}
	io.WriteString(conn, fmt.Sprintf("%s %d %s\n\n", HTTP_PROTOCOL, 200, "std-go/rpc based on http connected"))
	s.serveConn(conn)
}

func (s *Server) serveConn(conn net.Conn) {
	defer conn.Close()
	// decode Option by JSON
	opt := Option{}
	if err := json.NewDecoder(conn).Decode(&opt); err != nil {
		log.Println("std-go/rpc server: option error", err)
		return
	}
	if opt.Magic != MAGIC {
		log.Printf("std-go/rpc server: magic error(%d)\n", opt.Magic)
		return
	}
	f := codec.NewCodecConnFunc[opt.CodecType]
	if f == nil {
		log.Printf("std-go/rpc server: codec type error(%s)\n", opt.CodecType)
		return
	}
	// handle Requests
	s.serveCodecConn(f(conn), opt.HandleTimeout)
}

func (s *Server) serveCodecConn(codecConn codec.CodecConn, timeout time.Duration) {
	defer codecConn.Close()
	wg := &sync.WaitGroup{}
	mutex := &sync.Mutex{}
	for {
		req, err := s.readRequest(codecConn)
		if err != nil {
			if req == nil {
				// no more requests
				break
			}
			// bad request
			req.h.Error = err.Error()
			s.sendResponse(codecConn, req.h, "", mutex)
			continue
		}
		wg.Add(1)
		go s.handleRequest(codecConn, req, wg, mutex, timeout)
	}
	//  waiting for conn processing to end
	wg.Wait()
}

func (s *Server) parseServiceMethod(serviceMethod string) (*service, *method, error) {
	sm := strings.Split(serviceMethod, ".")
	service, ok := s.servicesMap[sm[0]]
	if !ok {
		return nil, nil, errors.New("invalid service")
	}
	method, ok := service.methodsMap[sm[1]]
	if !ok {
		return service, nil, errors.New("invalid method")
	}
	return service, method, nil
}

type request struct {
	h       *protoc.Header
	service *service
	method  *method
	args    reflect.Value
	reply   reflect.Value
}

func (s *Server) readRequest(codecConn codec.CodecConn) (*request, error) {
	var err error
	req := &request{}
	// read Header
	header := &protoc.Header{}
	if err = codecConn.ReadHeader(header); err != nil {
		if err != io.EOF && err != io.ErrUnexpectedEOF {
			log.Println("std-go/rpc server: header error:", err)
		}
		return nil, err
	}
	req.h = header
	// read Body
	if req.service, req.method, err = s.parseServiceMethod(req.h.ServiceMethod); err != nil {
		log.Println("std-go/rpc server: invalid service or method:", err)
		codecConn.ReadBody(nil)
		return req, err
	}
	req.args = req.method.newArg()
	req.reply = req.method.newReply()
	// use pointer to read args from body
	argp := req.args.Interface()
	if req.args.Type().Kind() != reflect.Pointer {
		argp = req.args.Addr().Interface()
	}
	if err := codecConn.ReadBody(argp); err != nil {
		log.Println("std-go/rpc server: args error:", err)
		return req, err
	}

	return req, nil
}

func (s *Server) handleRequest(codecConn codec.CodecConn, req *request, wg *sync.WaitGroup, mutex *sync.Mutex, timeout time.Duration) {
	defer wg.Done()
	called := make(chan struct{}, 1)
	go func(codecConn codec.CodecConn, req *request, mutex *sync.Mutex) {
		if err := req.service.call(req.method.name, req.args, req.reply); err != nil {
			req.h.Error = err.Error()
		}
		called <- struct{}{}
	}(codecConn, req, mutex)
	select {
	case <-time.After(timeout):
		req.h.Error = "server handle request timeout"
	case <-called:
		// do nothing
	}
	s.sendResponse(codecConn, req.h, req.reply.Interface(), mutex)
}

func (s *Server) sendResponse(codecConn codec.CodecConn, header *protoc.Header, body interface{}, mutex *sync.Mutex) {
	mutex.Lock()
	defer mutex.Unlock()
	if err := codecConn.Write(header, body); err != nil {
		log.Println("std-go/rpc server: send response error:", err)
	}
}
