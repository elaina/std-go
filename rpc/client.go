package rpc

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net"
	"net/http"
	"sync"
	"time"

	"gitlab.com/elaina/std-go/rpc/codec"
	"gitlab.com/elaina/std-go/rpc/protoc"
)

type Call struct {
	Seq           uint64
	ServiceMethod string
	Args          interface{}
	Reply         interface{}
	Error         error
	Done          chan *Call
}

func (c *Call) done() {
	c.Done <- c
}

type newClientResult struct {
	client *Client
	err    error
}

func DialHTTP(addr string, opt *Option) (*Client, error) {
	return DialHTTPURL(addr, HTTP_URL, opt)
}

func DialHTTPURL(addr string, url string, opt *Option) (*Client, error) {
	conn, err := net.DialTimeout("tcp", addr, opt.ConnectTimeout)
	if err != nil {
		return nil, err
	}
	io.WriteString(conn, fmt.Sprintf("%s %s %s\n\n", HTTP_METHOD, url, HTTP_PROTOCOL))
	resp, err := http.ReadResponse(bufio.NewReader(conn), &http.Request{Method: HTTP_METHOD})
	if err != nil {
		return nil, err
	}
	if resp.StatusCode != http.StatusOK {
		return nil, errors.New("unexpected status: " + resp.Status)
	}
	return NewClient(conn, opt)
}

func Dial(addr string, opt *Option) (*Client, error) {
	conn, err := net.DialTimeout("tcp", addr, opt.ConnectTimeout)
	if err != nil {
		return nil, err
	}
	return NewClient(conn, opt)
}

func NewClient(conn net.Conn, opt *Option) (*Client, error) {
	res := make(chan *newClientResult, 1)
	go func(conn net.Conn, opt *Option, res chan *newClientResult) {
		if err := json.NewEncoder(conn).Encode(opt); err != nil {
			res <- &newClientResult{client: nil, err: err}
			return
		}
		codecFunc := codec.NewCodecConnFunc[opt.CodecType]
		if codecFunc == nil {
			res <- &newClientResult{client: nil, err: errors.New("unsupported codec type")}
			return
		}
		client := &Client{
			opt:       opt,
			codecConn: codecFunc(conn),
			seq:       1,
			sending:   &sync.Mutex{},
			mu:        &sync.Mutex{},
			calls:     make(map[uint64]*Call),
		}
		res <- &newClientResult{client: client, err: nil}
	}(conn, opt, res)
	select {
	case <-time.After(opt.ConnectTimeout):
		return nil, errors.New("create client timeout")
	case r := <-res:
		if r.client != nil {
			go r.client.recieve()
		}
		return r.client, r.err
	}
}

type Client struct {
	opt       *Option
	codecConn codec.CodecConn
	seq       uint64
	sending   *sync.Mutex // sending request lock
	mu        *sync.Mutex // calls map operation lock
	calls     map[uint64]*Call
	closed    bool
}

func (c *Client) registerCall(call *Call) error {
	c.mu.Lock()
	defer c.mu.Unlock()
	if c.closed {
		return errors.New("rpc client shutdown")
	}
	call.Seq = c.seq
	c.calls[call.Seq] = call
	c.seq++
	return nil
}

func (c *Client) popCall(seq uint64) *Call {
	c.mu.Lock()
	defer c.mu.Unlock()
	call := c.calls[seq]
	delete(c.calls, call.Seq)
	return call
}

func (c *Client) cancelCall(err error) {
	c.sending.Lock() // unable to send new request
	defer c.sending.Unlock()
	c.mu.Lock() // unable to register new call
	defer c.mu.Unlock()
	for seq := range c.calls {
		c.calls[seq].Error = err
		c.calls[seq].done()
	}
}

func (c *Client) send(call *Call) {
	defer c.sending.Unlock()
	c.sending.Lock()
	h := protoc.Header{ServiceMethod: call.ServiceMethod, Seq: call.Seq}
	call.Error = c.codecConn.Write(&h, call.Args)
}

func (c *Client) recieve() {
	var err error
	for err == nil {
		h := protoc.Header{}
		if err := c.codecConn.ReadHeader(&h); err != nil {
			// EOF connection is closed, stop recieving response
			break
		}
		call := c.popCall(h.Seq)
		if call == nil {
			err = c.codecConn.ReadBody(nil)
			continue
		}
		if h.Error != "" {
			// rpc server error
			call.Error = errors.New(h.Error)
			err = c.codecConn.ReadBody(nil)
			call.done()
			continue
		}
		if err = c.codecConn.ReadBody(call.Reply); err != nil {
			// read reply error
			call.Error = err
		}
		call.done()
	}
	// cancel the remaining calls
	c.cancelCall(err)
}

func (c *Client) Go(serviceMethod string, args interface{}, reply interface{}) (*Call, error) {
	call := &Call{
		ServiceMethod: serviceMethod,
		Args:          args,
		Reply:         reply,
		Done:          make(chan *Call, 1),
	}
	if err := c.registerCall(call); err != nil {
		return nil, err
	}
	c.send(call)
	return call, nil
}

func (c *Client) Call(serviceMethod string, args interface{}, reply interface{}) error {
	call, err := c.Go(serviceMethod, args, reply)
	if err != nil {
		// send request failure, cancel this call
		c.popCall(call.Seq)
		return err
	}
	return (<-call.Done).Error
}

func (c *Client) Close() error {
	if err := c.codecConn.Close(); err != nil {
		return err
	}
	c.closed = true
	return nil
}

func (c *Client) IsAvailable() bool {
	return !c.closed
}
