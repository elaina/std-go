package dao

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

const (
	MYSQL string = "mysql"
)

func New(opt *Option) (*gorm.DB, error) {
	return gorm.Open(opt.Type, opt.GetURL())
}

func Close(db *gorm.DB) {
	if db != nil {
		db.Close()
	}
}
