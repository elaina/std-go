package router

import (
	"github.com/gin-gonic/gin"
)

type Router func(*gin.Engine)

var routers []Router

func Include(newRouters ...Router) {
	routers = append(routers, newRouters...)
}

func Init() *gin.Engine {
	engine := gin.Default()
	for _, router := range routers {
		router(engine)
	}
	return engine
}
