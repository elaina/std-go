package webserver

import (
	"fmt"
	"io"
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/elaina/std-go/webserver/router"
)

type Option struct {
	Port               int
	Log                string
	MaxMultipartMemory int64
	HTMLGlob           string
}

func Run(opt *Option, routers ...router.Router) {
	// 修改日志
	if opt.Log != "" {
		f, _ := os.Create(opt.Log)
		gin.DefaultWriter = io.MultiWriter(f)
	}
	// 注册路由
	router.Include(routers...)
	server := router.Init()
	// 限制请求表单大小，默认32MB
	if opt.MaxMultipartMemory != 0 {
		server.MaxMultipartMemory = opt.MaxMultipartMemory
	}
	// 加载模版
	if opt.HTMLGlob != "" {
		server.LoadHTMLGlob(opt.HTMLGlob)
	}
	server.Run(fmt.Sprintf(":%d", opt.Port))
}
